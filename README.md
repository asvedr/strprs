# STRPRS struct parser generator
## What is it?
Library for declarative parser generation via structs annotations

## Example: Parser for List of int
Declaration:
```go
import g "gitlab.com/asvedr/strprs/generics"

type Item struct {
    Value g.Int `spaceindented:"t"`
    Comma g.None `token:"," spaceindented:"t"`
}

type IntList struct {
    Begin g.None `token:"[" spaceindented:"t"`
    Data List[Item]
    End g.None `token:"]" spaceindented:"t"`
}

func(il IntList) Value() []int64 {
    var res []int64
    for _, item := range il.Data.Data {
        res = append(res, item.Value.Data)
    }
    return res
}
```
Usage:
```go
func TestIntList(t *testing.T) {
    prs, err := strprs.MakeParser[IntList]()
    assert.Nil(t, err)
    lst, err := prs(" [1,2, 3,\n]")
    assert.Nil(t, err)
    assert.Equal(t, lst.Value(), []int64{1,2,3})
}
```
## Features
- Declarative definition ([details](/docs/definition_rules.md))
- Recursive structs support([json parser definition in 95 lines](/tests/json_test/definition_test.go))
- Strict typed parsing result
- Indents support ([details](/docs/indents.md))
- Stadanrt generics(ints, reals, lists, opts) ([details](/docs/std_generics.md))
- Flexible rules ([details](/docs/custom_parser.md))
- Dymaic ranges/tokens ([details](/docs/ranges_and_tokens.md))
- "Any order" extractor 

## Documentation
Start reading [here](/docs/definition_rules.md)

package consts

const (
	MetaField             = "Meta"
	TagCustomParser       = "parser"
	TagCustomDependencies = "dependencies"
	TagMode               = "mode"
	TagTokenMethod        = "tokenmethod"
	TagTokenData          = "token"
	TagTokenName          = "tokenname"
	TagRangeMethod        = "rangemethod"
	TagRangeData          = "range"
	TagRangeName          = "rangename"
	TagRangeKey           = "rangekey"
	TagIfFollow           = "iffollow"
	TagNotParse           = "notparse"
	TagNotSave            = "notsave"
	TagFatalErr           = "fatal"
	TagMinLen             = "min"
	TagMaxLen             = "max"
	TagIgnore             = "ignore"
	TagSpaceIndented      = "spaceindented"
)

const (
	ModeSeq = "seq"
	// ModeOr       = "or"
	ModeAnyOrder = "anyorder"
)

const (
	Space   = " \n\t"
	KeyGte1 = "+"
	KeyGte0 = "*"
	KeyOne  = "1"
)

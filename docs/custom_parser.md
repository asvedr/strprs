# How to define a struct with a custom parser
Put method name in `parser` tag for `Meta` field(receiver must be ptr, result type must be same structure):
```go
import (
    g "gitlab.com/asvedr/strprs/generics"
    p "gitlab.com/asvedr/strprs/proto"
)

type Prs struct {
    Meta g.Meta `parse:"Method"`
}

func(*Prs) Method(reg p.IRegistry, req p.Request) p.Parsed {
    // your code
    return p.Parsed{
        Value: Prs{...},
        Cursor: ...,
        Err: ...,
    }
}
```

# Why must I declare tag? Can't you just check struct for "Parser" field or IDK?
Because explicit is better then implicit
```
# Definition rules

1. [How to define struct](#how-to-define-parsable-struct)
2. [What is meta field](#what-is-meta-field)
3. [Sequence mode](#seq-mode)
4. [Or mode](#or-mode)
5. [Any order mode](#anyorder-mode)
6. [PEG operations(or/list/opt/follow)](#peg-operations)
7. [All tags](#all-tags)

## How to define parsable struct
```go
import g "gitlab.com/asvedr/strprs"

type MyStruct struct {
    Meta g.Meta // special optional field for configuration
    A TypeA // data field1
    B TypeB // data field2
    ...
}
```
`A` and `B` are destination to parsring. After parsing you will go `MyStruct` with filled fields `A` and `B`. `Meta` field always contains default value.

## What is meta field
It's optional field that used only for tags that affected all the struct. Tags that are available for `Meta` field: `parser:"..." dependencies:"..." mode:"..."`
- ["parser" tag details](/docs/custom_parser.md)
- ["dependencies" tag details](/docs/dependencies.md)
- "mode" can be ["seq"](#seq-mode), ["or"(not implemented)](#or-mode), ["anyorder"](#anyorder-mode)

## Seq mode
Sequence mode is the default mode. In this mode struct fields are parsed consistently one by one. If any field-parser returns error then the struct parser returns error too. 
Example:
```go
type S struct {
    A string `token:"a" spaceindented:"t"`
    B string `token:"b" spaceindented:"t"`
}

...
prs, _ := strprs.MakeParser[S]()
prs("a b") => S{A: "a", B: "b"}
prs("b a") => error
...
```

## Or mode
Not implemented yet. For OR construction use [OR generics](/docs/or_generics.md)

## Anyorder mode
In this mode all data fields will be filled but the order doesn't matter. `anyorder(A,B,C) => ABC|ACB|BAC|BCA|CAB|CBA`. If any field-parser returns error then the struct parser returns error too.
```go
import g "gitlab.com/asvedr/strprs"

type S struct {
    Meta g.Meta `mode:"anyorder"`
    A string `token:"a" spaceindented:"t"`
    B string `token:"b" spaceindented:"t"`
}
...
prs, _ := strprs.MakeParser[S]()
prs("a b") => S{A: "a", B: "b"}
prs("b a") => S{A: "a", B: "b"}
prs("b x") => error
...
```

## PEG operations
PEG operations allows you to build complex rules:
- [one-or-more (+)](/docs/list_generics.md)
- [zero-or-mode (*)](/docs/list_generics.md)
- [optional (?)](/docs/opt_generics.md)
- [or operations (A|B)](/docs/or_generics.md)
- [if-follow/if-not-follow](docs/follow.md)

## All tags
`Meta` tags are described [here](#what-is-meta-field).

Field tags list
- `tokenmethod:"..."` method name that returns data for tokens [details](/docs/ranges_and_tokens.md)
- `token:"..."` data for token [details](/docs/ranges_and_tokens.md)
- `tokenname:"..."` name for dynamic token [details](/docs/ranges_and_tokens.md)
- `rangemethod:"..."` method name that returns data for range [details](/docs/ranges_and_tokens.md)
- `range:"..."` data for range [details](/docs/ranges_and_tokens.md)
- `rangename:"..."` name for dynamic range [details](/docs/ranges_and_tokens.md)
- `rangekey:"..."` ("+", "*" or "1") [details](/docs/ranges_and_tokens.md)
- `iffollow:"t"` iffollow mode for field [details](/docs/follow.md)
- `notparse:"t"` do not call parser of this field
- `notsave:"t"` call parser for field but not save result to field
- `fatal:"t"` error in this field will break all the parsing process
- `min:"123"` set min len for list [details](/docs/list_generics.md)
- `max:"123"` set max len for list [details](/docs/list_generics.md)
- `ignore:"t"` ignore field on all the stages of parser building. Use this tag if you define custom parser.
- `spaceindented:"t"` indent field with space

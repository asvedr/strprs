# How to correctly register struct in parser builder.
If you want to make parser for struct you have to make parser for all the fields obviously. How does it work?

When you call parser builder for struct, builder recursive check all the fields except `Meta` field and fields that have `ignore:"t"` tag. Example:
```go
import g "gitlab.com/asvedr/strprs"

type S[T any] struct {
    A g.Int
    B T
    X int `ignore:"t"`
}
```
Now if you call `strprs.MakeParser[S]()` the builder will automaticly collect `g.Int` and `T` types as dependencies and build parsers for them. But it will ignore `int` field.

If you have custom parser with implicit dependency from other structs you have to use `Meta` tag `dependencies:"..."`. Method must have ptr receiver. Example:
```go
import g "gitlab.com/asvedr/strprs"

type S[T any] struct {
    Meta g.Meta `dependencies:"Deps"`
}

func (*S[T]) Deps() []reflect.Type {
    var t T
    return []reflect.Type{reflect.TypeOf(t)}
}
```

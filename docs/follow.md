# What if I need to parse A only if B follow

There are two ways:
1. ```go
   type S struct {
       A Type1 `iffollow:"t"`
       B Type2
   }
   ```
2. ```go
   import g "gitlab.com/asvedr/strprs/generics"

   type S struct {
       A g.IfFollow[A, B]
   }
   ```

If you are using method with tag and you do not want to parse `B`. If you need `B` only as a condition for `A` you need to add tag `notparse:"t"` to `B`
```go
type S struct {
    A Type1 `iffollow:"t"`
    B Type2 `notparse:"t"`
}
```

# Special conditions for List and Opt
## List
```go
import g "gitlab.com/asvedr/strprs/generics"

type S struct {
    Lst g.List[g.SpaceIndented[g.Int]] `iffollow:"t"`
    End `token:"5" spaceindented:"t"`
}

...
prs, _ := strprs.MakeParser[S]
res, err := prs("1 2 3 4 5 6 7")
// res => S{Lst: [1,2,3,4], End: "5"}
```
## Opt
```go
import g "gitlab.com/asvedr/strprs/generics"

type Token struct {
    T bool `token:"no"`
}

type S struct {
    No g.Opt[Token] `iffollow:"t"`
    End `token:" "`
}

...
prs, _ := strprs.MakeParser[S]
res, err := prs("no x")
// res.No.Data.T == true
res, err := prs("not x")
// res.No.Data.T == false
```

# What if I need to parse A only if B NOT follow
**Not implemented yet!**
```go
type S struct {
    Field g.IfNotFollow[A, B]
}
```
parse `A` only if `B` not follow
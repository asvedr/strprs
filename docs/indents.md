# How to use indents
There are two indents _space_ and _custom_

## Two ways for space indents
- type `generics.SpaceIndented[T]`
- field tag `spaceindented:"t"`

Example:
```go
import g "gitlab.com/asvedr/strprs/generics"

type Str struct {
    A MyType `spaceindented:"t"`
    B g.SpaceIndented[MyType]
}
```
Both variants work similar.

## Custom intents
There is a generic for this case: `generics.Indented[Parser, Indent]`

Example:
```go
import g "gitlab.com/asvedr/strprs/generics"

type MyInd struct {
    Val string `range:"a-z"`
}

type Str {
    A g.Indented[MyType, MyInd]
}
```

After the parsing process `Str{}.A.Data` will contain the value

# List generic
List generic allows you to parse sequence of same item. You can **optionally** set minimum and maximim len. [Has a special behaviour for iffollow](/docs/follow.md)
```go
import g "gitlab.com/asvedr/strprs/generics"

// this parser can parse list of int separated with space
// Minimum list len is 1 and maximum is 10
type ListOfInt struct {
    Data g.List[g.SpaceIndented[g.Int]] `min:1 max:10`
}
```

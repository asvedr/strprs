# Opt generic
This generic parses its child if possible. Else child is empty. [Has a special behaviour with follow](/docs/follow.md)
```go
import g "gitlab.com/asvedr/strprs/generics"

type S struct {
    A g.Opt[g.Int]
    B string `range:"a-z" rangekey:"+"`
}

...
prs, _ := strprs.MakeParser[S]()
res, err := prs("123abc")
// *res.A.Data == 123, res.B == "abc"
res, err := prs("abc")
// res.A.Data == nil, res.B == "abc"
```

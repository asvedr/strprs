# Or expression via generics
This generic parses *only one* of variants. Allows you to make enums. `g.OrN.Data` is `any` and contains one of child.

Available *or* generics: `Or2`, `Or3`, `Or4`, `Or5`

Example:
```go
import g "gitlab.com/asvedr/strprs/generics"

type TokenA struct {
    T g.None `token:"a"`
}

type S struct {
    T g.Or3[TokenA, g.Real, g.Int]
}

...
prs, err := strprs.MakeParser[S]()
res, err := prs("a")
// res.T => TokenA
res, err := prs("123")
// res.T => g.Int{Data: 123}
res, err := prs("123.2")
// res.T => g.Real{Data: 123.2}
```

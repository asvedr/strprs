# Field type
Range and token field can be one of 3 types
- `string`: value will be fully saved
- `bool`: will be set as true on success
- `generics.None`: value will not be set

# Ranges
Range is sequence of char from one alphabet. Range can be defined via one of special tags (`range`, `rangemethod`, `rangename`) and `rangekey`.
- `range` set range as is. Example: `range:"a-z"` is all symbols from 'a' to 'z' include both. "a-zA-Z" is all latin symbols from both registers. "\<123>" - symbol #123.
- `rangemethod` set range via method name. Method must return full range as a string
- `rangename` set range via name. Value will be taken from *context* by name as a key
- `rangekey` can be "+", "*" or "1"

```go
type S struct {
    A string `range:"123" rangekey:"+"`
    B string `rangemethod:"M" rangekey:"*"`
}

func(*S) M() string {
    return "abcdef"
}
```

# Tokens
Tokens allows you to parse constant value. Token can be defined via one of special tags (`token`, `tokenmethod`, `tokenname`).
- `token` set token as is
- `tokenmethod` set token via method name. Method must return full token as a string
- `tokenname` set token via name. Value will be taken from *context* by name as a key

```go
type S struct {
    A string `token:"123"`
    B string `rangemethod:"M"`
}

func(*S) M() string {
    return "abcdef"
}
```
# All the standart generics

- `None` special value for ranges and tokens. Can not be parsed directly 
- `Empty` parse empty string. Can be used with iffollow or ifnotfollow
- `List[T]` T* and T+ [details](/docs/list_generics.md)
- `Opt[T]` T? [details](/docs/opt_generics.md)
- `Indented[T, Indent]` skip indent and save T [details](/docs/indents.md)
- `SpaceIndented[T]` skip space indent and save T [details](/docs/indents.md)
- `IntStr` integer value saved as string
- `Int` integer value saved as int64
- `RealStr` float value saved as string
- `Real` float value saved as float64
- `Or2` see [or generics](/docs/or_generics.md)
- `Or3` see [or generics](/docs/or_generics.md)
- `Or4` see [or generics](/docs/or_generics.md)
- `Or5` see [or generics](/docs/or_generics.md)

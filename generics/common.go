package generics

import (
	"reflect"
	"strconv"

	"gitlab.com/asvedr/strprs/consts"
	p "gitlab.com/asvedr/strprs/proto"
	"gitlab.com/asvedr/strprs/prserr"
)

type Meta struct{}

type None struct {
	Meta Meta `parser:"Parse"`
}

type Empty struct {
	Meta Meta `parser:"Parse"`
}

type Term struct {
	Meta Meta `parser:"Parse"`
}

type List[Parser any] struct {
	Meta Meta     `parser:"Parse"`
	Data []Parser `notparse:"t"`
}

type Opt[Parser any] struct {
	Meta Meta    `parser:"Parse"`
	Data *Parser `notparse:"t"`
}

type Indented[Parser, Indent any] struct {
	Meta Meta `parser:"Parse" dependencies:"Deps"`
	Data Parser
}

type SpaceIndented[Parser any] struct {
	Meta Meta `parser:"Parse"`
	Data Parser
}

type Int struct {
	Data string `range:"0-9" rangekey:"+"`
}

type Real struct {
	Int  Int
	Dot  string `token:"."`
	Frac Int
}

func (*None) Parse(reg p.IRegistry, req p.Request) p.Parsed {
	panic("call of None.Parse not expected")
}

func (*Empty) Parse(reg p.IRegistry, req p.Request) p.Parsed {
	return p.Parsed{
		Value:  Empty{},
		Cursor: req.Cursor,
	}
}

func (*Term) Parse(reg p.IRegistry, req p.Request) p.Parsed {
	if len(req.Runes) > 0 {
		err := req.NonFatal("expected end of string")
		return p.Parsed{Err: err}
	}
	return p.Parsed{Value: Term{}, Cursor: req.Cursor}
}

func (self *Indented[P, I]) Deps() []reflect.Type {
	var p *P
	var i *I
	return []reflect.Type{reflect.TypeOf(p).Elem(), reflect.TypeOf(i).Elem()}
}

func (self *Indented[P, I]) Parse(reg p.IRegistry, req p.Request) p.Parsed {
	indent_parser := p.GetParser[I](reg)
	ind_parsed := indent_parser.F(reg, req.DropParams())
	if ind_parsed.Err == nil {
		req.Cursor = ind_parsed.Cursor
	}
	item_parser := p.GetParser[P](reg)
	parsed := item_parser.F(reg, req)
	return p.Parsed{
		Value: Indented[P, I]{
			Data: parsed.Value.(P),
		},
		Cursor: parsed.Cursor,
		Err:    parsed.Err,
	}
}

func (self *SpaceIndented[P]) Parse(reg p.IRegistry, req p.Request) p.Parsed {
	indent_parser := reg.GetRangeParser(consts.Space, consts.KeyGte0)
	ind_parsed := indent_parser.F(reg, req.DropParams())
	if ind_parsed.Err == nil {
		req.Cursor = ind_parsed.Cursor
	}
	item_parser := p.GetParser[P](reg)
	parsed := item_parser.F(reg, req)
	if parsed.Err != nil {
		return p.Parsed{Err: parsed.Err}
	}
	return p.Parsed{
		Value:  SpaceIndented[P]{Data: parsed.Value.(P)},
		Cursor: parsed.Cursor,
	}
}

func (self Int) Into() (int64, error) {
	return strconv.ParseInt(self.Data, 10, 64)
}

func (self Real) Into() (float64, error) {
	return strconv.ParseFloat(self.Int.Data+"."+self.Frac.Data, 64)
}

func (self *Opt[T]) Parse(reg p.IRegistry, req p.Request) p.Parsed {
	item_parser := p.GetParser[T](reg)
	child_req := req.DropParams()
	parsed := item_parser.F(reg, child_req)
	if parsed.Err != nil {
		if parsed.Err.IsFatal() {
			return p.Parsed{Err: parsed.Err}
		} else {
			return p.Parsed{Cursor: req.Cursor}
		}
	}
	if req.Follow != nil && req.Follow.F(reg, child_req).Err != nil {
		return p.Parsed{Cursor: req.Cursor}
	}
	data := parsed.Value.(T)
	return p.Parsed{
		Cursor: parsed.Cursor,
		Value:  Opt[T]{Data: &data},
	}
}

func (self *List[T]) Parse(reg p.IRegistry, req p.Request) p.Parsed {
	items, crs, err := self.parse_items(reg, req)
	return p.Parsed{
		Cursor: crs,
		Value:  List[T]{Data: items},
		Err:    err,
	}
}

func (self *List[T]) parse_items(reg p.IRegistry, req p.Request) ([]T, p.Cursor, prserr.IError) {
	item_parser := p.GetParser[T](reg)
	var items []T
	child_req := req.DropParams()
	for i := 0; i < req.MinLen; i += 1 {
		parsed := item_parser.F(reg, child_req)
		if parsed.Err != nil {
			return items, child_req.Cursor, parsed.Err
		}
		child_req.Cursor = parsed.Cursor
		items = append(items, parsed.Value.(T))
	}
	for {
		if self.follow_found(reg, child_req) || self.max_reached(items, req) {
			break
		}
		parsed := item_parser.F(reg, child_req)
		if parsed.Err != nil {
			if !parsed.Err.IsFatal() {
				parsed.Err = nil
			}
			return items, child_req.Cursor, parsed.Err
		}
		child_req.Cursor = parsed.Cursor
		items = append(items, parsed.Value.(T))
	}
	return items, child_req.Cursor, nil
}

func (*List[T]) follow_found(reg p.IRegistry, req p.Request) bool {
	return req.Follow != nil && req.Follow.F(reg, req).Err == nil
}

func (*List[T]) max_reached(items []T, req p.Request) bool {
	return req.MaxLen != nil && len(items) > *req.MaxLen
}

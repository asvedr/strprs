package generics

import (
	"reflect"

	p "gitlab.com/asvedr/strprs/proto"
)

type IfFollow[T, F any] struct {
	Meta Meta `parser:"Parse" dependencies:"Deps"`
	Data T
}

type IfNotFollow[T, F any] struct {
	Meta Meta `parser:"Parse" dependencies:"Deps"`
	Data T
}

func (*IfFollow[T, F]) Deps() []reflect.Type {
	var t *T
	var f *F
	return []reflect.Type{reflect.TypeOf(t).Elem(), reflect.TypeOf(f).Elem()}
}

func (*IfFollow[T, F]) Parse(reg p.IRegistry, req p.Request) p.Parsed {
	t := p.GetParser[T](reg)
	f := p.GetParser[F](reg)
	parsed := t.F(
		reg,
		p.Request{Cursor: req.Cursor, Follow: &f},
	)
	if parsed.Err != nil {
		return parsed
	}
	follow := f.F(reg, p.Request{Cursor: parsed.Cursor})
	if follow.Err == nil {
		parsed.Value = IfFollow[T, F]{
			Data: parsed.Value.(T),
		}
		return parsed
	} else {
		err := parsed.NonFatal("expected " + f.Name)
		return p.Parsed{Err: err}
	}
}

func (*IfNotFollow[T, F]) Deps() []reflect.Type {
	var t *T
	var f *F
	return []reflect.Type{reflect.TypeOf(t).Elem(), reflect.TypeOf(f).Elem()}
}

func (*IfNotFollow[T, F]) Parse(reg p.IRegistry, req p.Request) p.Parsed {
	t := p.GetParser[T](reg)
	f := p.GetParser[F](reg)
	parsed := t.F(reg, req)
	if parsed.Err != nil {
		return parsed
	}
	follow := f.F(reg, p.Request{Cursor: parsed.Cursor})
	if follow.Err != nil {
		parsed.Value = IfNotFollow[T, F]{
			Data: parsed.Value.(T),
		}
		return parsed
	} else {
		err := parsed.NonFatal("not expected " + f.Name)
		return p.Parsed{Err: err}
	}
}

package generics

import (
	"fmt"
	"reflect"

	p "gitlab.com/asvedr/strprs/proto"
)

const IntoMethodName = "Into"

type Into[Src, Res any] struct {
	Meta  Meta `parser:"Parse" dependencies:"Deps"`
	Value Res  `ignore:"t"`
}

func (*Into[Src, Res]) Deps() []reflect.Type {
	var s *Src
	src := reflect.TypeOf(s).Elem()
	m, found := src.MethodByName(IntoMethodName)
	if !found {
		panic("struct " + src.String() + " method " + IntoMethodName + " not found")
	}
	expected := func(Src) (Res, error) { panic("call not expected") }
	if !m.Type.ConvertibleTo(reflect.TypeOf(expected)) {
		msg := fmt.Sprintf(
			"struct %s method %s has type %s, expected %s",
			src.String(),
			IntoMethodName,
			m.Type.String(),
			reflect.TypeOf(expected).String(),
		)
		panic(msg)
	}
	return []reflect.Type{src}
}

func (*Into[Src, Res]) Parse(reg p.IRegistry, req p.Request) p.Parsed {
	parser := p.GetParser[Src](reg)
	parsed := parser.F(reg, req)
	if parsed.Err != nil {
		return parsed
	}
	obj := reflect.ValueOf(parsed.Value)
	method := obj.MethodByName(IntoMethodName)
	res := method.Call(nil)
	if !res[1].IsNil() {
		msg := res[1].Interface().(error).Error()
		if req.FatalErr {
			parsed.Err = req.Fatal(msg)
		} else {
			parsed.Err = req.NonFatal(msg)
		}
		return parsed
	}
	val := res[0].Interface().(Res)
	parsed.Value = Into[Src, Res]{Value: val}
	return parsed
}

package generics

import (
	"reflect"

	p "gitlab.com/asvedr/strprs/proto"
)

type Or2[A, B any] struct {
	Meta Meta `parser:"Parse" dependencies:"Deps"`
	Data any
}

type Or3[A, B, C any] struct {
	Meta Meta `parser:"Parse" dependencies:"Deps"`
	Data any
}

type Or4[A, B, C, D any] struct {
	Meta Meta `parser:"Parse" dependencies:"Deps"`
	Data any
}

type Or5[A, B, C, D, E any] struct {
	Meta Meta `parser:"Parse" dependencies:"Deps"`
	Data any
}

func (self *Or2[A, B]) Deps() []reflect.Type {
	var a *A
	var b *B
	return []reflect.Type{
		reflect.TypeOf(a).Elem(),
		reflect.TypeOf(b).Elem(),
	}
}

func (self *Or2[A, B]) Parse(reg p.IRegistry, req p.Request) p.Parsed {
	return parse_or(
		reg,
		req,
		func(val any) any { return Or2[A, B]{Data: val} },
		p.GetParser[A](reg), p.GetParser[B](reg),
	)
}

func (self *Or3[A, B, C]) Deps() []reflect.Type {
	var a *A
	var b *B
	var c *C
	return []reflect.Type{
		reflect.TypeOf(a).Elem(),
		reflect.TypeOf(b).Elem(),
		reflect.TypeOf(c).Elem(),
	}
}

func (self *Or3[A, B, C]) Parse(reg p.IRegistry, req p.Request) p.Parsed {
	return parse_or(
		reg,
		req,
		func(val any) any { return Or3[A, B, C]{Data: val} },
		p.GetParser[A](reg),
		p.GetParser[B](reg),
		p.GetParser[C](reg),
	)
}

func (self *Or4[A, B, C, D]) Deps() []reflect.Type {
	var a *A
	var b *B
	var c *C
	var d *D
	return []reflect.Type{
		reflect.TypeOf(a).Elem(),
		reflect.TypeOf(b).Elem(),
		reflect.TypeOf(c).Elem(),
		reflect.TypeOf(d).Elem(),
	}
}

func (self *Or4[A, B, C, D]) Parse(reg p.IRegistry, req p.Request) p.Parsed {
	return parse_or(
		reg,
		req,
		func(val any) any { return Or4[A, B, C, D]{Data: val} },
		p.GetParser[A](reg),
		p.GetParser[B](reg),
		p.GetParser[C](reg),
		p.GetParser[D](reg),
	)
}

func (self *Or5[A, B, C, D, E]) Deps() []reflect.Type {
	var a *A
	var b *B
	var c *C
	var d *D
	var e *E
	return []reflect.Type{
		reflect.TypeOf(a).Elem(),
		reflect.TypeOf(b).Elem(),
		reflect.TypeOf(c).Elem(),
		reflect.TypeOf(d).Elem(),
		reflect.TypeOf(e).Elem(),
	}
}

func (self *Or5[A, B, C, D, E]) Parse(reg p.IRegistry, req p.Request) p.Parsed {
	return parse_or(
		reg,
		req,
		func(val any) any { return Or5[A, B, C, D, E]{Data: val} },
		p.GetParser[A](reg),
		p.GetParser[B](reg),
		p.GetParser[C](reg),
		p.GetParser[D](reg),
		p.GetParser[E](reg),
	)
}

func parse_or(
	reg p.IRegistry,
	req p.Request,
	init func(any) any,
	parsers ...p.Parser,
) p.Parsed {
	var res p.Parsed
	for _, parser := range parsers {
		res = parser.F(reg, req)
		if res.Err == nil {
			break
		}
	}
	if res.Err == nil {
		res.Value = init(res.Value)
	}
	return res
}

package app

import (
	"gitlab.com/asvedr/strprs/internal/fmb"
	fpb "gitlab.com/asvedr/strprs/internal/fpb/builder"
	"gitlab.com/asvedr/strprs/internal/fpb/rng"
	"gitlab.com/asvedr/strprs/internal/fpb/tkn"
	"gitlab.com/asvedr/strprs/internal/proto"
	"gitlab.com/asvedr/strprs/internal/smb"
	"gitlab.com/asvedr/strprs/internal/spb"
	p "gitlab.com/asvedr/strprs/proto"
)

func StructMetaBuilder() proto.IStructMetaBuilder {
	return smb.Builder[fmb.Builder]{}
}

func StructParserBuilder[R p.IRegistry](reg R) proto.IStructParserBuilder {
	return spb.New(fpb.New(rng.New(reg), tkn.New(reg), reg))
}

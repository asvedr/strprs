package fmb

// proto.IFieldMetaBuilder

import (
	"reflect"
	"strconv"

	"gitlab.com/asvedr/strprs/consts"
	"gitlab.com/asvedr/strprs/generics"
	"gitlab.com/asvedr/strprs/internal/types/field_meta"
)

type Builder struct{}

func (Builder) Build(
	field reflect.StructField,
) field_meta.Meta {
	flags := []uint8{}
	if get_bool_tag(field, consts.TagIfFollow) {
		flags = append(flags, field_meta.CheckFollow)
	}
	if get_bool_tag(field, consts.TagNotParse) {
		flags = append(flags, field_meta.NotParse)
	}
	if get_bool_tag(field, consts.TagNotSave) || tp_is_none(field) {
		flags = append(flags, field_meta.NotSave)
	}
	if get_bool_tag(field, consts.TagFatalErr) {
		flags = append(flags, field_meta.FatalErr)
	}
	if get_bool_tag(field, consts.TagIgnore) {
		flags = append(flags, field_meta.Ignore)
	}
	if get_bool_tag(field, consts.TagSpaceIndented) {
		flags = append(flags, field_meta.SpaceIndent)
	}
	p_min_len := get_opt_int_tag(field, consts.TagMinLen)
	min_len := 0
	if p_min_len != nil {
		min_len = *p_min_len
	}
	return field_meta.Meta{
		Name:  field.Name,
		Type:  field.Type,
		Token: get_token_meta(field),
		Range: get_range_meta(field),
		List: field_meta.List{
			MinLen: min_len,
			MaxLen: get_opt_int_tag(field, consts.TagMaxLen),
		},
		Flags: field_meta.NewFlags(flags...),
	}
}

func get_opt_int_tag(field reflect.StructField, name string) *int {
	val := field.Tag.Get(name)
	if val == "" {
		return nil
	}
	ival, err := strconv.Atoi(val)
	if err != nil {
		panic("tag '" + name + "' value must be int")
	}
	return &ival
}

func get_bool_tag(field reflect.StructField, name string) bool {
	return field.Tag.Get(name) != ""
}

func tp_is_none(field reflect.StructField) bool {
	return field.Type == reflect.TypeOf(generics.None{})
}

func get_token_meta(field reflect.StructField) *field_meta.RT {
	return get_rt_meta(
		field,
		consts.TagTokenMethod,
		consts.TagTokenName,
		consts.TagTokenData,
	)
}

func get_range_meta(field reflect.StructField) *field_meta.RT {
	rt := get_rt_meta(
		field,
		consts.TagRangeMethod,
		consts.TagRangeName,
		consts.TagRangeData,
	)
	if rt == nil {
		return rt
	}
	rt.Key = field.Tag.Get(consts.TagRangeKey)
	return rt
}

func get_rt_meta(
	field reflect.StructField,
	method_field string,
	name_field string,
	data_field string,
) *field_meta.RT {
	method := field.Tag.Get(method_field)
	if method != "" {
		return &field_meta.RT{Method: method}
	}
	name := field.Tag.Get(name_field)
	if name != "" {
		return &field_meta.RT{Name: name}
	}
	data := field.Tag.Get(data_field)
	if data != "" {
		return &field_meta.RT{Data: data}
	}
	return nil
}

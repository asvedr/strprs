package fpb

import (
	"gitlab.com/asvedr/strprs/consts"
	"gitlab.com/asvedr/strprs/internal/proto"
	"gitlab.com/asvedr/strprs/internal/types/field_meta"
	"gitlab.com/asvedr/strprs/internal/types/struct_meta"
	p "gitlab.com/asvedr/strprs/proto"
)

// proto.IFieldParserBuilder

type Builder[
	RangeBuilder proto.IFieldParserBuilder,
	TokenBuilder proto.IFieldParserBuilder,
	Reg p.IRegistry,
] struct {
	rng RangeBuilder
	tkn TokenBuilder
	reg Reg
}

func New[
	RB proto.IFieldParserBuilder,
	TB proto.IFieldParserBuilder,
	R p.IRegistry,
](rng RB, tkn TB, reg R) Builder[RB, TB, R] {
	return Builder[RB, TB, R]{rng: rng, tkn: tkn, reg: reg}
}

func (self Builder[RB, TB, R]) Build(
	str struct_meta.Meta,
	fld field_meta.Meta,
) (p.Parser, error) {
	f, err := self.get_value_func(str, fld)
	if err != nil {
		return p.Parser{}, err
	}
	decorated := decorate(f, fld)
	if fld.Flags.SpaceIndented() {
		decorated = space_indent(decorated)
	}
	return decorated, nil
}

func (self Builder[RB, TB, R]) get_value_func(
	str struct_meta.Meta,
	fld field_meta.Meta,
) (p.Parser, error) {
	if fld.Range != nil {
		return self.rng.Build(str, fld)
	}
	if fld.Token != nil {
		return self.tkn.Build(str, fld)
	}
	return self.reg.GetParser(fld.StructType()), nil
}

func decorate(
	orig p.Parser,
	meta field_meta.Meta,
) p.Parser {
	check_follow := meta.Flags.CheckFollow()
	fatal_err := meta.Flags.FatalErr()
	min_len := meta.List.MinLen
	max_len := meta.List.MaxLen
	f := func(reg p.IRegistry, req p.Request) p.Parsed {
		if !check_follow {
			req.Follow = nil
		}
		req.FatalErr = fatal_err
		req.MinLen = min_len
		req.MaxLen = max_len
		return orig.F(reg, req)
	}
	return p.Parser{Name: orig.Name, F: f}
}

func space_indent(orig p.Parser) p.Parser {
	f := func(reg p.IRegistry, req p.Request) p.Parsed {
		rng := reg.GetRangeParser(consts.Space, "*")
		indented := rng.F(reg, req)
		if indented.Err != nil {
			return indented
		}
		req.Cursor = indented.Cursor
		return orig.F(reg, req)
	}
	return p.Parser{Name: "space+" + orig.Name, F: f}
}

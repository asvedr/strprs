package rng

import (
	"fmt"
	"reflect"
	"sort"
	"strconv"

	"gitlab.com/asvedr/strprs/generics"
	"gitlab.com/asvedr/strprs/internal/types/field_meta"
	"gitlab.com/asvedr/strprs/internal/types/struct_meta"
	"gitlab.com/asvedr/strprs/proto"
)

const (
	state_init int = iota
	state_slash
	state_num
	state_minus
)

type Builder[R proto.IRegistry] struct {
	reg     R
	str_tp  reflect.Type
	bool_tp reflect.Type
	none_tp reflect.Type
}

type parser_state struct {
	runes         []rune
	state         int
	num           string
	minus_indexes []int
}

func New[R proto.IRegistry](reg R) *Builder[R] {
	return &Builder[R]{
		reg:     reg,
		str_tp:  reflect.TypeOf(""),
		bool_tp: reflect.TypeOf(true),
		none_tp: reflect.TypeOf(generics.None{}),
	}
}

func (self *Builder[R]) Build(
	str struct_meta.Meta,
	fld field_meta.Meta,
) (proto.Parser, error) {
	var src, key string
	if fld.Range.Method != "" {
		src, key = get_method_res(str, fld.Range.Method)
	} else {
		data, err := self.get_data(fld.Range.Name, fld.Range.Data)
		if err != nil {
			return proto.Parser{}, err
		}
		src = ParseRange(data)
	}
	if fld.Range.Key != "" {
		key = fld.Range.Key
	}
	switch fld.Type {
	case self.str_tp:
		return self.reg.GetRangeParser(src, key), nil
	case self.bool_tp:
		return with_res(self.reg.GetRangeParser(src, key), true), nil
	case self.none_tp:
		return self.reg.GetRangeParser(src, key), nil
	default:
		panic("invalid field type for range: " + fld.Type.Name())
	}
}

func (self Builder[R]) get_data(name string, data string) (string, error) {
	if name == "" {
		return data, nil
	}
	data, found := self.reg.GetRange(name)
	if !found {
		return "", fmt.Errorf("range '%s' not registered", name)
	}
	return data, nil
}

func with_res(prs proto.Parser, val any) proto.Parser {
	f := prs.F
	prs.F = func(reg proto.IRegistry, req proto.Request) proto.Parsed {
		parsed := f(reg, req)
		parsed.Value = val
		return parsed
	}
	return prs
}

func get_method_res(
	str struct_meta.Meta,
	name string,
) (string, string) {
	value := reflect.New(str.Type)
	_, found := str.Type.MethodByName(name)
	if !found {
		msg := fmt.Sprintf("struct '%s' method '%s' not found", str.Type.Name(), name)
		panic(msg)
	}
	result := value.MethodByName(name).Call([]reflect.Value{})
	data := result[0].Interface().(string)
	var key string
	if len(result) > 1 {
		key = result[1].Interface().(string)
	}
	return data, key
}

func ParseRange(src string) string {
	state := &parser_state{state: state_init}
	for _, sym := range src {
		switch state.state {
		case state_init:
			on_init(state, sym)
		case state_slash:
			on_slash(state, sym)
		case state_num:
			on_num(state, sym)
		case state_minus:
			on_minus(state, sym)
		}
	}
	if state.state != state_init {
		panic("Invalid range: \"" + src + "\"")
	}
	runes := deref_minuses(state)
	sort.Slice(runes, func(i, j int) bool { return runes[i] < runes[j] })
	var result string
	for _, sym := range runes {
		result += string(sym)
	}
	return result
}

func on_init(state *parser_state, sym rune) {
	switch sym {
	case '\\':
		state.state = state_slash
	case '-':
		state.state = state_minus
	default:
		state.runes = append(state.runes, sym)
	}
}

func on_slash(state *parser_state, sym rune) {
	switch sym {
	case '\\':
		state.runes = append(state.runes, '\\')
		state.state = state_init
	case 'n':
		state.runes = append(state.runes, '\n')
		state.state = state_init
	case 't':
		state.runes = append(state.runes, '\t')
		state.state = state_init
	case 'r':
		state.runes = append(state.runes, '\r')
		state.state = state_init
	case '-':
		state.runes = append(state.runes, '-')
		state.state = state_init
	case '<':
		state.state = state_num
		state.num = ""
	}
}

func on_num(state *parser_state, sym rune) {
	if sym >= '0' && sym <= '9' {
		state.num += string(sym)
		return
	}
	if sym != '>' {
		panic("invalid symbol after \\<: " + string(sym))
	}
	num, err := strconv.Atoi(state.num)
	if err != nil {
		panic("symbol code is not num: " + state.num)
	}
	state.runes = append(state.runes, rune(num))
	state.state = state_init
}

func on_minus(state *parser_state, sym rune) {
	state.minus_indexes = append(state.minus_indexes, len(state.runes))
	state.runes = append(state.runes, sym)
	state.state = state_init
}

func deref_minuses(state *parser_state) []rune {
	runes := state.runes
	indexes := state.minus_indexes
	if len(indexes) == 0 {
		return runes
	}
	if indexes[0] == 0 {
		panic("- can not be used in begin of range")
	}
	if indexes[len(indexes)-1] == len(runes) {
		panic("- can not be used in end of range")
	}
	result := []rune{}
	last_end := 0
	for _, i := range indexes {
		result = append(result, runes[last_end:i]...)
		for r := runes[i-1] + 1; r <= runes[i]; r += 1 {
			result = append(result, r)
		}
		last_end = i + 1
	}
	result = append(result, runes[last_end:]...)
	return result
}

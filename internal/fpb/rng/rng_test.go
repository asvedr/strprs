package rng_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/strprs/internal/fpb/rng"
)

func TestParse09(t *testing.T) {
	assert.Equal(
		t,
		"0123456789",
		rng.ParseRange("0-9"),
	)
}

func TestParseSyms(t *testing.T) {
	assert.Equal(
		t,
		"0123456789abcdef",
		rng.ParseRange("0-9a-f"),
	)
}

func TestParseSlash(t *testing.T) {
	assert.Equal(t, "-x", rng.ParseRange(`\-\<120>`))
}

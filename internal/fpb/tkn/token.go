package tkn

import (
	"fmt"
	"reflect"

	"gitlab.com/asvedr/strprs/generics"
	"gitlab.com/asvedr/strprs/internal/types/field_meta"
	"gitlab.com/asvedr/strprs/internal/types/struct_meta"
	"gitlab.com/asvedr/strprs/proto"
)

type Builder[R proto.IRegistry] struct {
	reg     R
	str_tp  reflect.Type
	bool_tp reflect.Type
	none_tp reflect.Type
}

func New[R proto.IRegistry](reg R) *Builder[R] {
	return &Builder[R]{
		reg:     reg,
		str_tp:  reflect.TypeOf(""),
		bool_tp: reflect.TypeOf(true),
		none_tp: reflect.TypeOf(generics.None{}),
	}
}

func (self *Builder[R]) Build(
	str struct_meta.Meta,
	fld field_meta.Meta,
) (proto.Parser, error) {
	var src string
	if fld.Token.Method != "" {
		src = get_method_res(str, fld.Token.Method)
	} else {
		data, err := self.get_data(fld.Token.Name, fld.Token.Data)
		if err != nil {
			return proto.Parser{}, err
		}
		src = data
	}
	switch fld.Type {
	case self.str_tp:
		return self.reg.GetTokenParser(src), nil
	case self.bool_tp:
		return with_res(self.reg.GetTokenParser(src), true), nil
	case self.none_tp:
		return self.reg.GetTokenParser(src), nil
	default:
		panic("invalid field type for token: " + fld.Type.Name())
	}
}

func (self Builder[R]) get_data(name string, data string) (string, error) {
	if name == "" {
		return data, nil
	}
	data, found := self.reg.GetToken(name)
	if !found {
		return "", fmt.Errorf("token '%s' not registered", name)
	}
	return data, nil
}

func with_res(prs proto.Parser, val any) proto.Parser {
	f := prs.F
	prs.F = func(reg proto.IRegistry, req proto.Request) proto.Parsed {
		parsed := f(reg, req)
		parsed.Value = val
		return parsed
	}
	return prs
}

func get_method_res(str struct_meta.Meta, name string) string {
	value := reflect.New(str.Type)
	_, found := str.Type.MethodByName(name)
	if !found {
		msg := fmt.Sprintf("struct '%s' method '%s' not found", str.Type.Name(), name)
		panic(msg)
	}
	result := value.MethodByName(name).Call([]reflect.Value{})
	data := result[0].Interface().(string)
	return data
}

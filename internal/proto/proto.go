package proto

import (
	"reflect"

	"gitlab.com/asvedr/strprs/internal/types/field_meta"
	"gitlab.com/asvedr/strprs/internal/types/struct_meta"
	p "gitlab.com/asvedr/strprs/proto"
)

type IFieldMetaBuilder interface {
	Build(field reflect.StructField) field_meta.Meta
}

type IStructMetaBuilder interface {
	Build(tp reflect.Type) struct_meta.Meta
}

type IFieldParserBuilder interface {
	Build(str struct_meta.Meta, fld field_meta.Meta) (p.Parser, error)
}

type IStructParserBuilder interface {
	Build(meta struct_meta.Meta) (p.Parser, error)
}

type IRangeParserBuilder interface {
	Build(rng string, key string) (p.Parser, error)
}

type ITokenParserBuilder interface {
	Build(data string) (p.Parser, error)
}

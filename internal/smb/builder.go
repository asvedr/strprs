package smb

// proto.IStructMetaBuilder

import (
	"fmt"
	"reflect"

	"gitlab.com/asvedr/strprs/consts"
	"gitlab.com/asvedr/strprs/internal/proto"
	"gitlab.com/asvedr/strprs/internal/types/field_meta"
	"gitlab.com/asvedr/strprs/internal/types/struct_meta"
)

type Builder[FB proto.IFieldMetaBuilder] struct {
	field_builder FB
}

func (self Builder[FB]) Build(t_strct reflect.Type) struct_meta.Meta {
	var meta struct_meta.Meta
	field0 := t_strct.Field(0)
	if field0.Name == consts.MetaField {
		meta = build_meta_with_field(t_strct, field0)
		meta.Fields = self.build_fields(t_strct, 1)
	} else {
		meta = struct_meta.Meta{}
		meta.Fields = self.build_fields(t_strct, 0)
	}
	meta.Type = t_strct
	// if meta.Mode == struct_meta.ModeOr {
	// 	ensure_fields_are_pointers(meta)
	// } else {
	// 	ensure_fields_are_not_pointers(meta)
	// }
	ensure_fields_are_not_pointers(meta)
	return meta
}

func (self Builder[FB]) build_fields(
	t_strct reflect.Type,
	i int,
) []field_meta.Meta {
	fields := []field_meta.Meta{}
	for ; i < t_strct.NumField(); i += 1 {
		f_meta := self.field_builder.Build(t_strct.Field(i))
		f_meta.Index = i
		if !f_meta.Flags.Ignore() {
			fields = append(fields, f_meta)
		}
	}
	return fields
}

func build_meta_with_field(
	t_strct reflect.Type,
	field0 reflect.StructField,
) struct_meta.Meta {
	return struct_meta.Meta{
		CustomParser: field0.Tag.Get(consts.TagCustomParser),
		CustomDeps:   field0.Tag.Get(consts.TagCustomDependencies),
		Mode:         parse_mode(t_strct, field0.Tag.Get(consts.TagMode)),
	}
}

// func ensure_fields_are_pointers(meta struct_meta.Meta) {
// 	for _, fld := range meta.Fields {
// 		if !fld.Flags.NotParse() && !fld.IsPtr() {
// 			msg := fmt.Sprintf(
// 				"field %s of OR struct %s is not a pointer",
// 				fld.Name,
// 				meta.Name,
// 			)
// 			panic(msg)
// 		}
// 	}
// }

func ensure_fields_are_not_pointers(meta struct_meta.Meta) {
	for _, fld := range meta.Fields {
		if !fld.Flags.NotParse() && fld.IsPtr() {
			msg := fmt.Sprintf(
				"field %s of struct %s is a pointer",
				fld.Name,
				meta.Type.String(),
			)
			panic(msg)
		}
	}
}

func parse_mode(strct reflect.Type, src string) struct_meta.Mode {
	switch src {
	case "":
		return struct_meta.ModeSeq
	case consts.ModeSeq:
		return struct_meta.ModeSeq
	// case consts.ModeOr:
	// 	return struct_meta.ModeOr
	case consts.ModeAnyOrder:
		return struct_meta.ModeAnyOrder
	default:
		panic("struct '" + strct.String() + "': invalid mode: " + src)
	}
}

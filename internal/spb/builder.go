package spb

import (
	"fmt"
	"reflect"

	"gitlab.com/asvedr/strprs/internal/proto"
	"gitlab.com/asvedr/strprs/internal/types/setter"
	"gitlab.com/asvedr/strprs/internal/types/struct_meta"
	p "gitlab.com/asvedr/strprs/proto"
	"gitlab.com/asvedr/strprs/prserr"
)

// proto.IStructParserBuilder

type Builder[FPB proto.IFieldParserBuilder] struct {
	field_builder FPB
	fun_type      reflect.Type
}

func sample_fun(reg p.IRegistry, req p.Request) p.Parsed {
	panic("should never call")
}

func New[FPB proto.IFieldParserBuilder](fpb FPB) Builder[FPB] {
	return Builder[FPB]{
		field_builder: fpb,
		fun_type:      reflect.TypeOf(sample_fun),
	}
}

func (self Builder[FPB]) Build(meta struct_meta.Meta) (p.Parser, error) {
	if meta.CustomParser != "" {
		return build_custom(meta, self.fun_type), nil
	}
	setters, err := self.make_setters(meta)
	if err != nil {
		return p.Parser{}, err
	}
	switch meta.Mode {
	case struct_meta.ModeSeq:
		return self.build_and(meta, setters), nil
	// case struct_meta.ModeOr:
	// 	return self.build_or(meta, setters)
	case struct_meta.ModeAnyOrder:
		return self.build_any_order(meta, setters), nil
	default:
		panic("struct '" + meta.Type.String() + "': unknown mode")
	}
}

func (self Builder[FPB]) make_setters(meta struct_meta.Meta) ([]setter.Setter, error) {
	var setters []setter.Setter
	for i, fld := range meta.Fields {
		if fld.Flags.NotParse() {
			continue
		}
		parser, err := self.field_builder.Build(meta, fld)
		if err != nil {
			return nil, err
		}
		req := setter.Request{Parser: parser, Meta: fld}
		if i+1 < len(meta.Fields) {
			follow, err := self.field_builder.Build(meta, meta.Fields[i+1])
			if err != nil {
				return nil, err
			}
			req.Follow = &follow
		}
		setters = append(setters, req.Make())
	}
	return setters, nil
}

/*
func (self Builder[FPB]) build_or(

	meta struct_meta.Meta,
	setters []setter.Setter,

	) p.Parser {
		tp := meta.Type
		f := func(reg p.IRegistry, req p.Request) p.Parsed {
			strct := reflect.New(tp)
			crs := req.Cursor
			var new_crs p.Cursor
			var err prserr.IError
			for _, setter := range setters {
				new_crs, err = setter(strct, reg, crs)
				if err == nil {
					return finalize(new_crs, strct)
				}
			}
			return p.Parsed{Err: err}
		}
		return p.Parser{Name: meta.Name, F: f}
	}
*/
func (self Builder[FPB]) build_and(
	meta struct_meta.Meta,
	setters []setter.Setter,
) p.Parser {
	tp := meta.Type
	f := func(reg p.IRegistry, req p.Request) p.Parsed {
		strct := reflect.New(tp)
		crs := req.Cursor
		var err prserr.IError
		for _, setter := range setters {
			crs, err = setter.F(strct, reg, crs)
			if err != nil {
				return p.Parsed{Err: err}
			}
		}
		return finalize(crs, strct)
	}
	return p.Parser{Name: meta.Type.Name(), F: f}
}

func (self Builder[FPB]) build_any_order(
	meta struct_meta.Meta,
	setters []setter.Setter,
) p.Parser {
	tp := meta.Type
	f := func(reg p.IRegistry, req p.Request) p.Parsed {
		strct := reflect.New(tp)
		crs, err := apply_any_order(strct, reg, req.Cursor, setters)
		if err != nil {
			return p.Parsed{Err: err}
		}
		return finalize(crs, strct)
	}
	return p.Parser{Name: meta.Type.Name(), F: f}
}

func apply_any_order(
	strct reflect.Value,
	reg p.IRegistry,
	crs p.Cursor,
	setters []setter.Setter,
) (p.Cursor, prserr.IError) {
	var err prserr.IError
	var res p.Cursor
	for i, stt := range setters {
		rest := []setter.Setter{}
		rest = append(rest, setters[:i]...)
		rest = append(rest, setters[i+1:]...)
		res, err = apply_any_order_on(strct, reg, crs, stt, rest)
		if err == nil {
			break
		}
	}
	return res, err
}

func apply_any_order_on(
	strct reflect.Value,
	reg p.IRegistry,
	crs p.Cursor,
	setter setter.Setter,
	rest []setter.Setter,
) (p.Cursor, prserr.IError) {
	crs, err := setter.F(strct, reg, crs)
	if err != nil {
		return crs, err
	}
	return apply_any_order(strct, reg, crs, rest)
}

func finalize(crs p.Cursor, strct reflect.Value) p.Parsed {
	return p.Parsed{
		Cursor: crs,
		Value:  strct.Elem().Interface(),
	}
}

func build_custom(
	meta struct_meta.Meta,
	fun_type reflect.Type,
) p.Parser {
	t := reflect.New(meta.Type)
	method := t.MethodByName(meta.CustomParser)
	if method.Kind() == reflect.Invalid {
		msg := fmt.Sprintf(
			"struct %s, method %s not found",
			meta.Type.String(),
			meta.CustomParser,
		)
		panic(msg)
	}
	if !method.Type().ConvertibleTo(fun_type) {
		msg := fmt.Sprintf(
			"struct %s, method %s has type %s, expected %s",
			meta.Type.String(),
			meta.CustomParser,
			method.Type().String(),
			fun_type.String(),
		)
		panic(msg)
	}
	f := func(reg p.IRegistry, req p.Request) p.Parsed {
		args := []reflect.Value{
			reflect.ValueOf(reg),
			reflect.ValueOf(req),
		}
		res_slice := method.Call(args)
		res_val := res_slice[0]
		as_any := res_val.Interface()
		return as_any.(p.Parsed)
	}
	return p.Parser{Name: meta.Type.Name(), F: f}
}

package field_meta

import "reflect"

const (
	CheckFollow uint8 = 0b000001
	NotParse    uint8 = 0b000010
	NotSave     uint8 = 0b000100
	FatalErr    uint8 = 0b001000
	Ignore      uint8 = 0b010000
	SpaceIndent uint8 = 0b100000
)

type Meta struct {
	Name  string
	Index int
	Type  reflect.Type
	Token *RT
	Range *RT
	List  List
	Flags Flags
}

type List struct {
	MinLen int
	MaxLen *int
}

type Flags struct {
	data uint8
}

// If .Method == "" then use .Data
type RT struct {
	Method string
	Name   string
	Data   string
	Key    string
}

func (self *Meta) StructType() reflect.Type {
	if self.IsPtr() {
		return self.Type.Elem()
	}
	if self.Type.Kind() == reflect.Slice {
		return self.Type.Elem()
	}
	return self.Type
}

func (self *Meta) IsPtr() bool {
	return self.Type.Kind() == reflect.Pointer
}

func NewFlags(flags ...uint8) Flags {
	var data uint8
	for _, flag := range flags {
		data = data | flag
	}
	return Flags{data: data}
}

func (f Flags) CheckFollow() bool {
	return f.data&CheckFollow != 0
}

func (f Flags) NotParse() bool {
	return f.data&NotParse != 0
}

func (f Flags) NotSave() bool {
	return f.data&NotSave != 0
}

func (f Flags) FatalErr() bool {
	return f.data&FatalErr != 0
}

func (f Flags) Ignore() bool {
	return f.data&Ignore != 0
}

func (f Flags) SpaceIndented() bool {
	return f.data&SpaceIndent != 0
}

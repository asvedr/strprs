package setter

import (
	"reflect"

	"gitlab.com/asvedr/strprs/internal/types/field_meta"
	p "gitlab.com/asvedr/strprs/proto"
	"gitlab.com/asvedr/strprs/prserr"
)

type Setter struct {
	Field string
	F     func(reflect.Value, p.IRegistry, p.Cursor) (p.Cursor, prserr.IError)
}

func (s Setter) String() string {
	return "<" + s.Field + ">"
}

type Request struct {
	Parser p.Parser
	Follow *p.Parser
	Meta   field_meta.Meta
}

func (self Request) Make() Setter {
	if self.Meta.Flags.Ignore() {
		panic("'ignore' field was not filtered")
	}
	parser := self.Parser
	follow := self.Follow
	is_ptr := self.Meta.IsPtr()
	fld_index := self.Meta.Index
	not_save := self.Meta.Flags.NotSave()
	tp := self.Meta.Type
	f := func(strct reflect.Value, reg p.IRegistry, crs p.Cursor) (p.Cursor, prserr.IError) {
		req := p.Request{Cursor: crs, Follow: follow}
		parsed := parser.F(reg, req)
		if parsed.Err != nil || not_save {
			return parsed.Cursor, parsed.Err
		}
		value := reflect.ValueOf(parsed.Value)
		if is_ptr {
			// value = reflect.ValueOf(parsed.Value).Addr()
			ptr := reflect.New(tp)
			// ptr.Set(value)
			ptr.Elem().Addr().Set(value)
			// ptr.Elem().Set(value)
			value = ptr
		}
		fld := strct.Elem().Field(fld_index)
		if parsed.Value == nil {
			fld.SetZero()
		} else {
			fld.Set(value)
		}
		return parsed.Cursor, nil
	}
	return Setter{Field: self.Meta.Name, F: f}
}

package struct_meta

import (
	"fmt"
	"reflect"

	"gitlab.com/asvedr/strprs/internal/types/field_meta"
)

type Mode uint8

const (
	ModeSeq Mode = iota
	// ModeOr
	ModeAnyOrder
)

type Meta struct {
	Type         reflect.Type
	CustomParser string
	CustomDeps   string
	Mode         Mode
	Fields       []field_meta.Meta
}

func (self Meta) CollectDependencies() []reflect.Type {
	if self.CustomDeps == "" {
		return self.get_field_deps()
	} else {
		return self.get_custom_deps()
	}
}

func (self Meta) get_field_deps() []reflect.Type {
	types := []reflect.Type{}
	for _, field := range self.Fields {
		tp := field.StructType()
		if tp.Kind() == reflect.Struct {
			types = append(types, tp)
		}
	}
	return types
}

func (self Meta) get_custom_deps() []reflect.Type {
	val := reflect.New(self.Type)
	method := val.MethodByName(self.CustomDeps)
	if method.Kind() == reflect.Invalid {
		msg := fmt.Sprintf(
			"struct %s method %s not found",
			self.Type.String(),
			self.CustomDeps,
		)
		panic(msg)
	}
	res := method.Call(nil)
	return res[0].Interface().([]reflect.Type)
}

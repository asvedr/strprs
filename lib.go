package strprs

import (
	"reflect"

	"gitlab.com/asvedr/strprs/internal/app"
	"gitlab.com/asvedr/strprs/processor"
	"gitlab.com/asvedr/strprs/proto"
	"gitlab.com/asvedr/strprs/prserr"
	"gitlab.com/asvedr/strprs/registry"
)

type Maker struct {
	reg  proto.IRegistry
	proc proto.IStructProcessor
}

type MakerParams struct {
	Ranges map[string]string
	Tokens map[string]string
}

func (self MakerParams) Build() Maker {
	reg := registry.New(self.Ranges, self.Tokens)
	proc := processor.Processor[*registry.Registry]{
		Reg:           reg,
		MetaBuilder:   app.StructMetaBuilder(),
		ParserBuilder: app.StructParserBuilder(reg),
	}
	return Maker{
		reg:  reg,
		proc: &proc,
	}
}

func MakeParserWithMaker[T any](
	maker Maker,
) (func(string) (T, prserr.IError), error) {
	var ptr *T
	tp := reflect.TypeOf(ptr).Elem()
	parser, err := maker.proc.Process(tp)
	if err != nil {
		return nil, err
	}
	return wrap[T](maker.reg, parser), nil
}

func MakeParser[T any]() (func(string) (T, prserr.IError), error) {
	maker := MakerParams{}.Build()
	var ptr *T
	tp := reflect.TypeOf(ptr).Elem()
	parser, err := maker.proc.Process(tp)
	if err != nil {
		return nil, err
	}
	return wrap[T](maker.reg, parser), nil
}

func wrap[T any](reg proto.IRegistry, parser proto.Parser) func(string) (T, prserr.IError) {
	return func(src string) (T, prserr.IError) {
		runes := []rune(src)
		crs := proto.Cursor{Runes: runes, Row: 1, Column: 1}
		req := proto.Request{Cursor: crs}
		parsed := parser.F(reg, req)
		var result T
		if parsed.Err != nil {
			return result, parsed.Err
		}
		result = parsed.Value.(T)
		return result, nil
	}
}

package processor

import (
	"reflect"

	internal "gitlab.com/asvedr/strprs/internal/proto"
	"gitlab.com/asvedr/strprs/proto"
)

type Processor[R proto.IRegistry] struct {
	Reg           R
	MetaBuilder   internal.IStructMetaBuilder
	ParserBuilder internal.IStructParserBuilder
}

func (self *Processor[R]) Process(strct_tp reflect.Type) (proto.Parser, error) {
	err := self.rec_down(strct_tp)
	if err != nil {
		return proto.Parser{}, err
	}
	return self.Reg.GetParser(strct_tp), nil
}

func (self *Processor[R]) rec_down(strct_tp reflect.Type) error {
	if self.Reg.CheckRegistred(strct_tp) {
		return nil
	}
	self.Reg.RegisterStructParser(strct_tp, make_rec_plug(strct_tp))
	str_meta := self.MetaBuilder.Build(strct_tp)
	for _, dep := range str_meta.CollectDependencies() {
		self.rec_down(dep)
	}
	parser, err := self.ParserBuilder.Build(str_meta)
	if err != nil {
		return err
	}
	self.Reg.RegisterStructParser(strct_tp, parser)
	return nil
}

func make_rec_plug(str reflect.Type) proto.Parser {
	str_name := str.String()
	name := str_name + "[rec]"
	f := func(reg proto.IRegistry, req proto.Request) proto.Parsed {
		got := reg.GetParser(str)
		if got.Name == name {
			panic("parser plug found loop on " + str_name)
		}
		return got.F(reg, req)
	}
	return proto.Parser{Name: name, F: f}
}

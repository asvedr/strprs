package proto

import (
	"reflect"
	"strings"

	"gitlab.com/asvedr/strprs/consts"
	"gitlab.com/asvedr/strprs/prserr"
)

type None struct{}

type Cursor struct {
	Runes  []rune
	Row    uint32
	Column uint32
}

type Request struct {
	Cursor
	Follow   *Parser
	FatalErr bool
	MinLen   int
	MaxLen   *int
}

type Parser struct {
	Name string
	F    func(IRegistry, Request) Parsed
}

type Parsed struct {
	Cursor
	Value any
	Err   prserr.IError
}

type IRegistry interface {
	CheckRegistred(reflect.Type) bool
	RegisterStructParser(reflect.Type, Parser)
	GetRangeParser(rng string, key string) Parser
	GetTokenParser(token string) Parser
	GetParser(reflect.Type) Parser
	GetRange(name string) (string, bool)
	GetToken(name string) (string, bool)
}

type IStructProcessor interface {
	Process(strct_tp reflect.Type) (Parser, error)
}

func (req Request) DropParams() Request {
	return Request{Cursor: req.Cursor}
}

func GetParser[T any](reg IRegistry) Parser {
	var ptr *T
	return reg.GetParser(reflect.TypeOf(ptr).Elem())
}

func (self *Cursor) Inc(sym rune) {
	if strings.ContainsRune(consts.Space, sym) {
		self.Row += 1
		self.Column = 1
	} else {
		self.Column += 1
	}
}

func (self *Cursor) ShiftRunes(n int) {
	self.Runes = self.Runes[n:]
}

func (self *Cursor) Fatal(msg string) prserr.IError {
	return prserr.NewFatal(self.Row, self.Column, msg)
}

func (self *Cursor) NonFatal(msg string) prserr.IError {
	return prserr.NewNonFatal(self.Row, self.Column, msg)
}

package prserr

type IError interface {
	error
	IsFatal() bool
	ToFatal() IError
	Row() uint32
	Column() uint32
}

type BaseErr struct {
	row    uint32
	column uint32
	msg    string
}

type NonFatalErr struct {
	BaseErr
}

type FatalErr struct {
	BaseErr
}

func NewNonFatal(
	row uint32,
	column uint32,
	msg string,
) IError {
	return &NonFatalErr{
		BaseErr: BaseErr{
			row:    row,
			column: column,
			msg:    msg,
		},
	}
}

func NewFatal(
	row uint32,
	column uint32,
	msg string,
) IError {
	return &FatalErr{
		BaseErr: BaseErr{
			row:    row,
			column: column,
			msg:    msg,
		},
	}
}

func (self *BaseErr) Error() string {
	return self.msg
}

func (self *BaseErr) Row() uint32 {
	return self.row
}

func (self *BaseErr) Column() uint32 {
	return self.column
}

func (self *BaseErr) ToFatal() IError {
	return &FatalErr{BaseErr: *self}
}

func (*NonFatalErr) IsFatal() bool {
	return false
}

func (*FatalErr) IsFatal() bool {
	return true
}

package registry

import (
	"reflect"

	"gitlab.com/asvedr/strprs/consts"
	"gitlab.com/asvedr/strprs/proto"
	"gitlab.com/asvedr/strprs/prserr"
)

type Registry struct {
	ranges      map[string]string
	tokens      map[string]string
	str_parsers map[reflect.Type]proto.Parser
	rng_parsers map[string]proto.Parser
	tkn_parsers map[string]proto.Parser
}

func New(
	ranges map[string]string,
	tokens map[string]string,
) *Registry {
	if ranges == nil {
		ranges = map[string]string{}
	}
	if tokens == nil {
		tokens = map[string]string{}
	}
	return &Registry{
		tokens:      tokens,
		ranges:      ranges,
		str_parsers: map[reflect.Type]proto.Parser{},
		rng_parsers: map[string]proto.Parser{},
		tkn_parsers: map[string]proto.Parser{},
	}
}

func (self *Registry) GetRange(name string) (string, bool) {
	val, found := self.ranges[name]
	return val, found
}

func (self *Registry) GetToken(name string) (string, bool) {
	val, found := self.tokens[name]
	return val, found
}

func (self *Registry) CheckRegistred(tp reflect.Type) bool {
	_, found := self.str_parsers[tp]
	return found
}

func (self *Registry) RegisterStructParser(tp reflect.Type, parser proto.Parser) {
	self.str_parsers[tp] = parser
}

func (self *Registry) GetParser(tp reflect.Type) proto.Parser {
	return self.str_parsers[tp]
}

func (self *Registry) GetRangeParser(rng string, key string) proto.Parser {
	name := rng + ":" + key
	prs, found := self.rng_parsers[name]
	if found {
		return prs
	}
	prs = make_range(name, rng, key)
	self.rng_parsers[name] = prs
	return prs
}

func (self *Registry) GetTokenParser(token string) proto.Parser {
	prs, found := self.tkn_parsers[token]
	if found {
		return prs
	}
	prs = make_token("token:"+token, token)
	self.tkn_parsers[token] = prs
	return prs
}

func make_token(name, token string) proto.Parser {
	runes := []rune(token)
	f := func(reg proto.IRegistry, req proto.Request) proto.Parsed {
		crs := req.Cursor
		err := func() proto.Parsed {
			err := req.NonFatal("expected token '" + token + "'")
			return proto.Parsed{Err: err}
		}
		if len(crs.Runes) < len(runes) {
			return err()
		}
		for i, sym := range runes {
			if crs.Runes[i] != sym {
				return err()
			} else {
				crs.Inc(sym)
			}
		}
		crs.ShiftRunes(len(runes))
		return proto.Parsed{
			Cursor: crs,
			Value:  token,
		}
	}
	return proto.Parser{Name: name, F: f}
}

func make_range(name, rng, key string) proto.Parser {
	set := map[rune]bool{}
	for _, sym := range rng {
		set[sym] = true
	}
	var f func(proto.IRegistry, proto.Request) proto.Parsed
	switch key {
	case consts.KeyGte0:
		f = make_gte_0(set)
	case consts.KeyGte1:
		f = make_gte_1(name, set)
	case consts.KeyOne:
		f = make_one(name, set)
	default:
		panic("invalid range key: '" + key + "'")
	}
	return proto.Parser{Name: name, F: f}
}

func make_gte_0(
	set map[rune]bool,
) func(proto.IRegistry, proto.Request) proto.Parsed {
	return func(reg proto.IRegistry, req proto.Request) proto.Parsed {
		return range_loop(set, req)
	}
}

func make_gte_1(
	rng string, set map[rune]bool,
) func(proto.IRegistry, proto.Request) proto.Parsed {
	return func(reg proto.IRegistry, req proto.Request) proto.Parsed {
		if len(req.Runes) == 0 || !set[req.Runes[0]] {
			return proto.Parsed{Err: req.NonFatal("expected " + rng)}
		}
		return range_loop(set, req)
	}
}

func range_loop(set map[rune]bool, req proto.Request) proto.Parsed {
	var i int
	var sym rune
	var result string
	shift_at := len(req.Runes)
	for i, sym = range req.Runes {
		if !set[sym] {
			shift_at = i
			break
		}
		result += string(sym)
		req.Inc(sym)
	}
	req.ShiftRunes(shift_at)
	return proto.Parsed{
		Cursor: req.Cursor,
		Value:  result,
	}
}

func make_one(
	rng string, set map[rune]bool,
) func(proto.IRegistry, proto.Request) proto.Parsed {
	return func(reg proto.IRegistry, req proto.Request) proto.Parsed {
		if len(req.Runes) == 0 || !set[req.Runes[0]] {
			err := prserr.NewNonFatal(
				req.Row,
				req.Column,
				"expected symbol from "+rng,
			)
			return proto.Parsed{Err: err}
		}
		crs := req.Cursor
		sym := (crs.Runes[0])
		crs.Inc(sym)
		crs.ShiftRunes(1)
		return proto.Parsed{Cursor: crs, Value: string(sym)}
	}
}

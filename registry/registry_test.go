package registry_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	p "gitlab.com/asvedr/strprs/proto"
	"gitlab.com/asvedr/strprs/registry"
)

func TestRangeMid(t *testing.T) {
	reg := registry.New(nil, nil)
	prs := reg.GetRangeParser("123456", "+")
	req := p.Request{Cursor: p.Cursor{Runes: []rune("43 x")}}
	parsed := prs.F(reg, req)
	assert.Equal(t, parsed.Value.(string), "43")
	assert.Equal(t, parsed.Cursor.Runes, []rune{' ', 'x'})
}

func TestRangeEnd(t *testing.T) {
	reg := registry.New(nil, nil)
	prs := reg.GetRangeParser("123456", "+")
	req := p.Request{Cursor: p.Cursor{Runes: []rune("43")}}
	parsed := prs.F(reg, req)
	assert.Equal(t, parsed.Value.(string), "43")
	assert.Equal(t, parsed.Cursor.Runes, []rune{})
}

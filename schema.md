Struct with completely custom parser
```go
type Parser struct {
    Meta generics.Meta `custom_parser:"Method"`
    FieldA TypeA
    FieldB TypeB
    ...
}

func (*Parser) Method(Cursor) Parsed[Parser] {...}
```

Available word methods: "+", "*", ""
Method "" mean that only one symbol is available
```go
type Parser struct {
    Meta generics.Meta `name:"struct-name"`
    TokenA string `token-method:"Tmethod"`
    WordA string `range:"a-z", method:"+"`
    // symbols ",`,\n,\
    WordB string `range:"\34\96\\\n", method:"*"`
    WordC string `range-method: "Rmethod`
}

func (*Parser) Tmethod() string {
    return "token-value"
}

func (*Parser) Rmethod() string {
    return "abcderfghijkl"
}
```

If follow
```go
type Parser struct {
    WordA string `range:"a-z", method:"+", iffollow:"t"`
    Token string `token:"abc", notparse:"t"`
}
```

Not save token
```go
type Parser struct {
    Token string `token:"abc", notsave:"t"`
    Num int
}
```

Space indent
```go
type Parser struct {
    A generics.Indented[generics.Int, generics.Space],
    B generics.SpaceIndented[generics.Int],
}
```

Or mode
```go
type Parser struct {
    Meta `or:"t"`
    Variant1 ...
    Variant2 ...
}
```

package any_order_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/strprs"
	g "gitlab.com/asvedr/strprs/generics"
)

type WithKey[Key any] struct {
	Key g.SpaceIndented[Key]
	Sep g.None `token:":"`
	Val g.Into[g.Int, int64]
}

type TokenH struct {
	T string `token:"h"`
}

type TokenM struct {
	T string `token:"m"`
}

type TokenS struct {
	T string `token:"s"`
}

type Time struct {
	Meta g.Meta `mode:"anyorder"`
	H    WithKey[TokenH]
	M    WithKey[TokenM]
	S    WithKey[TokenS]
}

type TimeValue struct {
	H int
	M int
	S int
}

func (self Time) Value() TimeValue {
	return TimeValue{
		H: int(self.H.Val.Value),
		M: int(self.M.Val.Value),
		S: int(self.S.Val.Value),
	}
}

func TestOrder1(t *testing.T) {
	prs, _ := strprs.MakeParser[Time]()
	time, err := prs("h:1 m:2 s:3")
	assert.Nil(t, err)
	assert.Equal(
		t,
		time.Value(),
		TimeValue{H: 1, M: 2, S: 3},
	)
}

func TestOrder2(t *testing.T) {
	prs, _ := strprs.MakeParser[Time]()
	time, err := prs("m:2 h:1 s:3")
	assert.Nil(t, err)
	assert.Equal(
		t,
		time.Value(),
		TimeValue{H: 1, M: 2, S: 3},
	)
}

func TestOrderErr(t *testing.T) {
	prs, _ := strprs.MakeParser[Time]()
	_, err := prs("m:2 s:3")
	assert.NotNil(t, err)
	_, err = prs("m:2 h::1 s:3")
	assert.NotNil(t, err)
}

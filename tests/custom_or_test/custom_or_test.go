package custom_or_test

import (
	g "gitlab.com/asvedr/strprs/generics"
)

type A struct {
	T bool `token:"a"`
}

type B struct {
	T bool `token:"b"`
}

type Enum struct {
	Meta g.Meta `mode:"or"`
	A    *A
	B    *B
}

/*
func TestEnum(t *testing.T) {
	f := strprs.MakeParser[Enum]()

	res, err := f("a")
	assert.Nil(t, err)
	assert.True(t, res.A.T)
	assert.Nil(t, res.B)

	// res, err = f("b")
	// assert.Nil(t, err)
	// assert.Nil(t, res.A)
	// assert.True(t, res.B.T)

	// res, err = f(".")
	// assert.NotNil(t, err)
	// assert.Nil(t, res.A)
	// assert.Nil(t, res.B)
}
*/

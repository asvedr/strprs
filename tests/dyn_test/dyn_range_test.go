package dyn_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/strprs"
)

type Word struct {
	W string `rangename:"word" rangekey:"+"`
}

func TestDynRangeA(t *testing.T) {
	maker := strprs.MakerParams{
		Ranges: map[string]string{"word": "a-c"},
	}.Build()
	prs, err := strprs.MakeParserWithMaker[Word](maker)
	assert.Nil(t, err)
	res, err := prs("cabde")
	assert.Nil(t, err)
	assert.Equal(t, res.W, "cab")
}

func TestDynRangeB(t *testing.T) {
	maker := strprs.MakerParams{
		Ranges: map[string]string{"word": "a-d"},
	}.Build()
	prs, err := strprs.MakeParserWithMaker[Word](maker)
	assert.Nil(t, err)
	res, err := prs("cabde")
	assert.Nil(t, err)
	assert.Equal(t, res.W, "cabd")
}

func TestDynRangeErr(t *testing.T) {
	maker := strprs.MakerParams{
		Ranges: map[string]string{"abc": "a-d"},
	}.Build()
	_, err := strprs.MakeParserWithMaker[Word](maker)
	assert.NotNil(t, err)
}

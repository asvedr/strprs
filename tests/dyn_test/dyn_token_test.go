package dyn_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/strprs"
)

type Token struct {
	W string `tokenname:"word"`
}

func TestDynTokenA(t *testing.T) {
	maker := strprs.MakerParams{
		Tokens: map[string]string{"word": "abc"},
	}.Build()
	prs, err := strprs.MakeParserWithMaker[Token](maker)
	assert.Nil(t, err)
	res, err := prs("abcde")
	assert.Nil(t, err)
	assert.Equal(t, res.W, "abc")
}

func TestDynTokenB(t *testing.T) {
	maker := strprs.MakerParams{
		Tokens: map[string]string{"word": "abcd"},
	}.Build()
	prs, err := strprs.MakeParserWithMaker[Token](maker)
	assert.Nil(t, err)
	res, err := prs("abcde")
	assert.Nil(t, err)
	assert.Equal(t, res.W, "abcd")
}

func TestDynTokenErr(t *testing.T) {
	maker := strprs.MakerParams{
		Tokens: map[string]string{"abc": "xyz"},
	}.Build()
	_, err := strprs.MakeParserWithMaker[Token](maker)
	assert.NotNil(t, err)
}

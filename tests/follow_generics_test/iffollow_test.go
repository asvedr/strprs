package follow_generics_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/strprs"
	g "gitlab.com/asvedr/strprs/generics"
)

func TestIfFollow(t *testing.T) {
	prs, err := strprs.MakeParser[g.IfFollow[g.Int, g.Term]]()
	assert.Nil(t, err)

	res, err := prs("123")
	assert.Nil(t, err)
	assert.Equal(t, res.Data.Data, "123")

	_, err = prs("123 ")
	assert.NotNil(t, err)
}

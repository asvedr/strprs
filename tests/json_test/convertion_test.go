package json_test

import g "gitlab.com/asvedr/strprs/generics"

func (self Atom) Value() any {
	data := self.Data.Data
	switch spec := data.(type) {
	case Null:
		return nil
	case String:
		return spec.Data
	case g.Into[g.Int, int64]:
		return int(spec.Value)
	case g.Into[g.Real, float64]:
		return spec.Value
	default:
		panic("")
	}
}

func (self *Node) Value() any {
	data := self.Data.Data
	switch spec := data.(type) {
	case Atom:
		return spec.Value()
	case List:
		return spec.Value()
	case Dict:
		return spec.Value()
	default:
		panic("")
	}
}

func (self List) Value() []any {
	res := []any{}
	for _, item := range self.Items.Data {
		res = append(res, item.Node.Value())
	}
	return res
}

func (self Dict) Value() map[string]any {
	res := map[string]any{}
	for _, item := range self.Items.Data {
		key := item.Key.Data
		val := item.Val.Value()
		res[key] = val
	}
	return res
}

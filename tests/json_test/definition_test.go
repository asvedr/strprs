package json_test

import (
	g "gitlab.com/asvedr/strprs/generics"
	p "gitlab.com/asvedr/strprs/proto"
)

type String struct {
	Meta g.Meta `parser:"Parse"`
	Data string `ignore:"t"`
}

func (self *String) Parse(reg p.IRegistry, req p.Request) p.Parsed {
	runes := req.Runes
	if len(runes) == 0 || runes[0] != '"' {
		return p.Parsed{Err: req.NonFatal("expected \"")}
	}
	state := ' '
	res := ""
	crs := req.Cursor
	crs.Inc('"')
	escaped := map[rune]rune{'n': '\n', 'r': '\r', 't': '\t', 'f': '\f'}
	for i, sym := range runes[1:] {
		crs.Inc(sym)
		switch state {
		case ' ':
			switch sym {
			case '\\':
				state = '\\'
			case '"':
				crs.ShiftRunes(i + 2)
				return p.Parsed{Cursor: crs, Value: String{Data: res}}
			default:
				res += string(sym)
			}
		case '\\':
			esc, found := escaped[sym]
			if !found {
				esc = sym
			}
			res += string(esc)
		}
	}
	return p.Parsed{Err: req.Fatal("string with no end")}
}

type Null struct {
	T g.None `token:"null"`
}

type Atom struct {
	Data g.Or4[
		Null,
		String,
		g.Into[g.Real, float64],
		g.Into[g.Int, int64],
	]
}

type ListItem struct {
	Node Node
	End  g.Or2[Comma, ListEnd]
}

type Comma struct {
	Comma g.None `token:"," spaceindented:"t"`
}

type ListEnd struct {
	Empty g.Empty `iffollow:"t"`
	End   g.None  `token:"]" notparse:"t"`
}

type DictEnd struct {
	Empty g.Empty `iffollow:"t"`
	End   g.None  `token:"}" notparse:"t"`
}

type List struct {
	Begin g.None `token:"["`
	Items g.List[ListItem]
	End   g.None `token:"]" spaceindented:"t"`
}

type DictItem struct {
	Key String `spaceindented:"t"`
	Sep g.None `token:":" spaceindented:"t"`
	Val Node
	End g.Or2[Comma, DictEnd]
}

type Dict struct {
	Begin g.None `token:"{"`
	Items g.List[DictItem]
	End   g.None `token:"}" spaceindented:"t"`
}

type Node struct {
	Data g.Or3[Atom, List, Dict] `spaceindented:"t"`
}

package json_test

import (
	"math"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/strprs"
)

func TestStr(t *testing.T) {
	prs, _ := strprs.MakeParser[Node]()
	res, err := prs(` "abc" `)
	assert.Nil(t, err)
	assert.Equal(t, res.Value(), "abc")
}

func TestInt(t *testing.T) {
	prs, _ := strprs.MakeParser[Node]()
	res, err := prs(`453`)
	assert.Nil(t, err)
	assert.Equal(t, res.Value(), 453)
}

func TestReal(t *testing.T) {
	prs, _ := strprs.MakeParser[Node]()
	res, err := prs(`4.5`)
	assert.Nil(t, err)
	fl, casted := res.Value().(float64)
	assert.True(t, casted)
	assert.True(t, math.Abs(fl-4.5) < 0.01)
}

func TestList(t *testing.T) {
	prs, _ := strprs.MakeParser[Node]()
	res, err := prs(`[1,2, 3]`)
	assert.Nil(t, err)
	assert.Equal(t, res.Value(), []any{1, 2, 3})
	res, err = prs(`[1,2, 3,]`)
	assert.Nil(t, err)
	assert.Equal(t, res.Value(), []any{1, 2, 3})
}

func TestDict(t *testing.T) {
	prs, _ := strprs.MakeParser[Node]()
	res, err := prs(
		`{"a":1, "b": "c", "d": [], "e": {"x": "y",}, "f": null}`,
	)
	assert.Nil(t, err)
	assert.Equal(
		t,
		res.Value(),
		map[string]any{
			"a": 1,
			"b": "c",
			"d": []any{},
			"e": map[string]any{"x": "y"},
			"f": nil,
		},
	)
}

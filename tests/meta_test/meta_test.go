package meta_test

import (
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"
	g "gitlab.com/asvedr/strprs/generics"
	"gitlab.com/asvedr/strprs/internal/app"
	"gitlab.com/asvedr/strprs/internal/types/field_meta"
	"gitlab.com/asvedr/strprs/internal/types/struct_meta"
)

type IndentedInt struct {
	Meta g.Meta
	Data g.SpaceIndented[g.Int]
}

type IndentedIntDefName struct {
	Data g.SpaceIndented[g.Int] `fatal:"t"`
}

// type Or struct {
// 	Meta g.Meta `mode:"or"`
// 	A    *g.Int
// 	B    *string `token:"stop"`
// }

type AnyOrder struct {
	Meta g.Meta `mode:"anyorder"`
	A    string `token:"hi"`
	B    string `token:"my"`
	C    string `token:"name"`
}

type Range struct {
	A string `rangemethod:"M"`
	B string `range:"a-f" rangekey:"+"`
}

type Flags struct {
	A g.Opt[g.Int] `iffollow:"t" fatal:"t"`
	B g.Int        `notparse:"t" notsave:"t"`
}

type Lst struct {
	A g.List[IndentedInt] `min:"1"`
	B g.List[IndentedInt] `max:"5"`
}

func TestIndentedInt(t *testing.T) {
	tp := reflect.TypeOf(IndentedInt{})
	meta := app.StructMetaBuilder().Build(tp)
	expected := struct_meta.Meta{
		Type: tp,
		Fields: []field_meta.Meta{
			{
				Name:  "Data",
				Index: 1,
				Type:  reflect.TypeOf(g.SpaceIndented[g.Int]{}),
			},
		},
	}
	assert.Equal(t, expected, meta)
}

func TestIndentedIntDefName(t *testing.T) {
	tp := reflect.TypeOf(IndentedIntDefName{})
	meta := app.StructMetaBuilder().Build(tp)
	expected := struct_meta.Meta{
		Type: tp,
		Fields: []field_meta.Meta{
			{
				Name:  "Data",
				Index: 0,
				Type:  reflect.TypeOf(g.SpaceIndented[g.Int]{}),
				Flags: field_meta.NewFlags(field_meta.FatalErr),
			},
		},
	}
	assert.Equal(t, expected, meta)
}

func TestCustomParser(t *testing.T) {
	tp := reflect.TypeOf(g.Into[g.Int, int64]{})
	meta := app.StructMetaBuilder().Build(tp)
	expected := struct_meta.Meta{
		Type:         tp,
		CustomParser: "Parse",
		CustomDeps:   "Deps",
		Fields:       []field_meta.Meta{},
	}
	assert.Equal(t, expected, meta)
}

func TestParametrized(t *testing.T) {
	tp := reflect.TypeOf(g.List[g.Int]{})
	meta := app.StructMetaBuilder().Build(tp)
	expected := struct_meta.Meta{
		Type:         tp,
		CustomParser: "Parse",
		Fields: []field_meta.Meta{
			{
				Name:  "Data",
				Index: 1,
				Type:  reflect.TypeOf([]g.Int{}),
				Flags: field_meta.NewFlags(field_meta.NotParse),
			},
		},
	}
	assert.Equal(t, expected, meta)
}

/*
func TestOr(t *testing.T) {
	tp := reflect.TypeOf(Or{})
	meta := app.StructMetaBuilder().Build(tp)
	expected := struct_meta.Meta{
		Name: "meta_test.Or",
		Type: tp,
		Mode: struct_meta.ModeOr,
		Fields: []field_meta.Meta{
			{
				Name:  "A",
				Index: 1,
				Type:  reflect.TypeOf(Or{}.A),
			},
			{
				Name:  "B",
				Index: 2,
				Type:  reflect.TypeOf(Or{}.B),
				Token: &field_meta.RT{Data: "stop"},
			},
		},
	}
	assert.Equal(t, expected, meta)
}
*/

func TestAnyOrder(t *testing.T) {
	tp := reflect.TypeOf(AnyOrder{})
	meta := app.StructMetaBuilder().Build(tp)
	expected := struct_meta.Meta{
		Type: tp,
		Mode: struct_meta.ModeAnyOrder,
		Fields: []field_meta.Meta{
			{
				Name:  "A",
				Index: 1,
				Type:  reflect.TypeOf(AnyOrder{}.A),
				Token: &field_meta.RT{Data: "hi"},
			},
			{
				Name:  "B",
				Index: 2,
				Type:  reflect.TypeOf(AnyOrder{}.B),
				Token: &field_meta.RT{Data: "my"},
			},
			{
				Name:  "C",
				Index: 3,
				Type:  reflect.TypeOf(AnyOrder{}.C),
				Token: &field_meta.RT{Data: "name"},
			},
		},
	}
	assert.Equal(t, expected, meta)
}

func TestRange(t *testing.T) {
	tp := reflect.TypeOf(Range{})
	meta := app.StructMetaBuilder().Build(tp)
	expected := struct_meta.Meta{
		Type: tp,
		Fields: []field_meta.Meta{
			{
				Name:  "A",
				Index: 0,
				Type:  reflect.TypeOf(""),
				Range: &field_meta.RT{Method: "M"},
			},
			{
				Name:  "B",
				Index: 1,
				Type:  reflect.TypeOf(""),
				Range: &field_meta.RT{Data: "a-f", Key: "+"},
			},
		},
	}
	assert.Equal(t, expected, meta)
}

func TestFlags(t *testing.T) {
	tp := reflect.TypeOf(Flags{})
	meta := app.StructMetaBuilder().Build(tp)
	expected := struct_meta.Meta{
		Type: tp,
		Fields: []field_meta.Meta{
			{
				Name:  "A",
				Index: 0,
				Type:  reflect.TypeOf(Flags{}.A),
				Flags: field_meta.NewFlags(field_meta.CheckFollow, field_meta.FatalErr),
			},
			{
				Name:  "B",
				Index: 1,
				Type:  reflect.TypeOf(Flags{}.B),
				Flags: field_meta.NewFlags(field_meta.NotParse, field_meta.NotSave),
			},
		},
	}
	assert.Equal(t, expected, meta)
}

func TestLst(t *testing.T) {
	tp := reflect.TypeOf(Lst{})
	meta := app.StructMetaBuilder().Build(tp)
	n5 := 5
	expected := struct_meta.Meta{
		Type: tp,
		Fields: []field_meta.Meta{
			{
				Name:  "A",
				Index: 0,
				Type:  reflect.TypeOf(Lst{}.A),
				List:  field_meta.List{MinLen: 1},
			},
			{
				Name:  "B",
				Index: 1,
				Type:  reflect.TypeOf(Lst{}.B),
				List:  field_meta.List{MaxLen: &n5},
			},
		},
	}
	assert.Equal(t, expected, meta)
}

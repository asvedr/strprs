package std_or_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/strprs"
	g "gitlab.com/asvedr/strprs/generics"
)

type A struct {
	T g.None `token:"a"`
}
type B struct {
	T g.None `token:"b"`
}
type C struct {
	T g.None `token:"c"`
}
type D struct {
	T g.None `token:"d"`
}
type E struct {
	T g.None `token:"e"`
}

func TestOr2(t *testing.T) {
	f, _ := strprs.MakeParser[g.Or2[A, B]]()

	res, err := f("a")
	assert.Nil(t, err)
	_, casted := res.Data.(A)
	assert.True(t, casted)

	res, err = f("b")
	assert.Nil(t, err)
	_, casted = res.Data.(B)
	assert.True(t, casted)

	res, err = f(".")
	assert.NotNil(t, err)
}

func TestOr3(t *testing.T) {
	f, _ := strprs.MakeParser[g.Or3[A, B, C]]()

	res, err := f("a")
	assert.Nil(t, err)
	_, casted := res.Data.(A)
	assert.True(t, casted)

	res, err = f("b")
	assert.Nil(t, err)
	_, casted = res.Data.(B)
	assert.True(t, casted)

	res, err = f("c")
	assert.Nil(t, err)
	_, casted = res.Data.(C)
	assert.True(t, casted)

	res, err = f(".")
	assert.NotNil(t, err)
}

func TestOr4(t *testing.T) {
	f, _ := strprs.MakeParser[g.Or4[A, B, C, D]]()

	res, err := f("a")
	assert.Nil(t, err)
	_, casted := res.Data.(A)
	assert.True(t, casted)

	res, err = f("b")
	assert.Nil(t, err)
	_, casted = res.Data.(B)
	assert.True(t, casted)

	res, err = f("c")
	assert.Nil(t, err)
	_, casted = res.Data.(C)
	assert.True(t, casted)

	res, err = f("d")
	assert.Nil(t, err)
	_, casted = res.Data.(D)
	assert.True(t, casted)

	res, err = f(".")
	assert.NotNil(t, err)
}

func TestOr5(t *testing.T) {
	f, _ := strprs.MakeParser[g.Or5[A, B, C, D, E]]()

	res, err := f("a")
	assert.Nil(t, err)
	_, casted := res.Data.(A)
	assert.True(t, casted)

	res, err = f("b")
	assert.Nil(t, err)
	_, casted = res.Data.(B)
	assert.True(t, casted)

	res, err = f("c")
	assert.Nil(t, err)
	_, casted = res.Data.(C)
	assert.True(t, casted)

	res, err = f("d")
	assert.Nil(t, err)
	_, casted = res.Data.(D)
	assert.True(t, casted)

	res, err = f("e")
	assert.Nil(t, err)
	_, casted = res.Data.(E)
	assert.True(t, casted)

	res, err = f(".")
	assert.NotNil(t, err)
}

package std_parsers_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/strprs"
	g "gitlab.com/asvedr/strprs/generics"
)

type IndInt struct {
	Data g.SpaceIndented[g.Into[g.Int, int64]]
}

type ListInt struct {
	Data g.List[IndInt]
}

type TWhats struct {
	T string `token:"whats"`
}
type TYour struct {
	T string `token:"your"`
}
type TName struct {
	T string `token:"name"`
}

type TokenBool struct {
	T bool `token:"abc"`
}

type TokenNone struct {
	T g.None `token:"abc"`
}

type WhatsYourName struct {
	Whats g.Opt[g.SpaceIndented[TWhats]]
	Your  g.Opt[g.SpaceIndented[TYour]]
	Name  g.Opt[g.SpaceIndented[TName]]
}

type Indented struct {
	Data g.Indented[g.Int, TName]
}

func (self IndInt) Into() (int, error) {
	return int(self.Data.Data.Value), nil
}

func (self ListInt) Into() ([]int, error) {
	var res []int
	for _, val := range self.Data.Data {
		i, _ := val.Into()
		res = append(res, i)
	}
	return res, nil
}

func TestParseIntOk(t *testing.T) {
	f, _ := strprs.MakeParser[g.Into[g.Int, int64]]()
	res, err := f("123")
	assert.Nil(t, err)
	assert.Equal(t, int(res.Value), 123)
}

func TestParseIntErr(t *testing.T) {
	f, _ := strprs.MakeParser[g.Into[g.Int, int64]]()
	_, err := f("  123")
	assert.NotNil(t, err)
}

func TestParseIndInt(t *testing.T) {
	f, _ := strprs.MakeParser[g.Into[IndInt, int]]()
	res, err := f("  123")
	assert.Nil(t, err)
	assert.Equal(t, res.Value, 123)
}

func TestParseListInt(t *testing.T) {
	f, _ := strprs.MakeParser[g.Into[ListInt, []int]]()
	res, err := f("1 2  3")
	assert.Nil(t, err)
	assert.Equal(t, res.Value, []int{1, 2, 3})
}

func TestOpt(t *testing.T) {
	f, _ := strprs.MakeParser[WhatsYourName]()

	res, err := f("whats your name?")
	assert.Nil(t, err)
	assert.NotNil(t, res.Whats.Data)
	assert.NotNil(t, res.Your.Data)
	assert.NotNil(t, res.Name.Data)

	res, err = f("your name whats")
	assert.Nil(t, err)
	assert.Nil(t, res.Whats.Data)
	assert.NotNil(t, res.Your.Data)
	assert.NotNil(t, res.Name.Data)
}

func TestReal(t *testing.T) {
	f, _ := strprs.MakeParser[g.Into[g.Real, float64]]()
	val, err := f("12.5")
	assert.Nil(t, err)
	assert.Equal(t, int(val.Value*10.0), 125)
}

func TestTokenBool(t *testing.T) {
	f, _ := strprs.MakeParser[TokenBool]()

	val, err := f("abc")
	assert.Nil(t, err)
	assert.True(t, val.T)

	val, err = f("123")
	assert.NotNil(t, err)
	assert.False(t, val.T)
}

func TestTokenNone(t *testing.T) {
	f, _ := strprs.MakeParser[TokenNone]()

	val, err := f("abc")
	assert.Nil(t, err)
	assert.Equal(t, val.T, g.None{})

	val, err = f("123")
	assert.NotNil(t, err)
	assert.Equal(t, val.T, g.None{})
}
